# Onionperf monit configuration

This repository contains a configuration file for
[monit](https://mmonit.com/monit/).  We use monit to monitor our
[onionperf](https://gitlab.torproject.org/tpo/metrics/onionperf)
deployments.

## Set up a monit instance

1. Install [monit](https://mmonit.com/monit/).

2. Adapt the monit configuration file from this repository to your needs.  You
   will have to create the file `monit-email-config`, which contains monit's
   SMTP configuration to send outage alerts.  The comment in the configuration
   file goes into more detail.

3. Start monit using this repository's configuration file:

         /path/to/monit -c /path/to/monitrc

4. Optionally: Make sure that your monit instance is started automatically after
   a reboot, e.g., by running `crontab -e` and adding the following line:

         @reboot /path/to/monit -c /path/to/monitrc
